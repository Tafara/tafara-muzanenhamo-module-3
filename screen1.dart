import 'package:flutter/material.dart';
import 'package:mtn_toth/dashboard.dart';

class Screen1 extends StatelessWidget {
  const Screen1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Screen1"),
          centerTitle: true,
        ),

        //body1
        body: Center(
            child: ElevatedButton(
          child: Text("Dashboard"),
          onPressed: () => {
            Navigator.push(context,
                new MaterialPageRoute(builder: (context) => Dashboard()))
          },
        )));
  }
}
