import 'package:flutter/material.dart';
import 'package:mtn_toth/loginandregistration.dart';
import 'package:mtn_toth/screen1.dart';
import 'package:mtn_toth/screen2.dart';
import 'package:mtn_toth/userprofileedit.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Dashboard"),
          centerTitle: true,
        ),

        //body
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
              ElevatedButton(
                child: Text("Profile Edit"),
                onPressed: () => {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => userProfileEdit()))
                },
              ),
              SizedBox(height: 5),
              // Button2

              ElevatedButton(
                child: Text("Screen1"),
                onPressed: () => {
                  Navigator.push(context,
                      new MaterialPageRoute(builder: (context) => Screen1()))
                },
              ),
              SizedBox(height: 5),
              // Button 3
              ElevatedButton(
                child: Text("Screen2"),
                onPressed: () => {
                  Navigator.push(context,
                      new MaterialPageRoute(builder: (context) => Screen2()))
                },
              ),
              SizedBox(height: 5),
              // Button 4
              ElevatedButton(
                child: Text("Log In"),
                onPressed: () => {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => LoginAndRegistration()))
                },
              )
            ])));
  }
}
