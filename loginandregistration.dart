import 'package:flutter/material.dart';
import 'package:mtn_toth/dashboard.dart';
import 'package:mtn_toth/registration.dart';

class LoginAndRegistration extends StatelessWidget {
  const LoginAndRegistration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Login and Registration"),
          centerTitle: true,
        ),
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
              // Button 1

              ElevatedButton(
                  child: Text("Register"),
                  onPressed: () => {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Registration())),
                      }),
              SizedBox(height: 5),
              // Button 2
              ElevatedButton(
                child: Text("Log In"),
                onPressed: () => {
                  Navigator.push(context,
                      new MaterialPageRoute(builder: (context) => Dashboard()))
                },
              ),
            ])));
  }
}

