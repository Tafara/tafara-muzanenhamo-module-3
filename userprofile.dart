import 'package:flutter/material.dart';
import 'package:mtn_toth/dashboard.dart';


class userProfileEdit extends StatelessWidget {
  const userProfileEdit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("User Profile Edit"),
          centerTitle: true,
        ),
        body: Center(
            child: ElevatedButton(
          onPressed: () => {
            Navigator.push(context,
                new MaterialPageRoute(builder: (context) => Dashboard()))
          },
          child: const Text("Dashboard"),
        )));
  }
}
