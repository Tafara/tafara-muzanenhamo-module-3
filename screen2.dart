import 'package:flutter/material.dart';
import 'package:mtn_toth/dashboard.dart';


class Screen2 extends StatelessWidget {
  const Screen2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Screen2"),
          centerTitle: true,
        ),

        //body1
        body: Center(
            child: ElevatedButton(
          child: Text("Dashboard"),
          onPressed: () => {
            Navigator.push(context,
                new MaterialPageRoute(builder: (context) => Dashboard()))
          },
        )));
  }
}
