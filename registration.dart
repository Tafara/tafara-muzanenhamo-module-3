import 'package:flutter/material.dart';
import 'package:mtn_toth/loginandregistration.dart';

class Registration extends StatelessWidget {
  const Registration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Registration"),
          centerTitle: true,
        ),
        body: Center(
            child: ElevatedButton(
          onPressed: () => {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) => LoginAndRegistration()))
          },
          child: const Text("Log In"),
        )));
  }
}
