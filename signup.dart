import 'package:flutter/material.dart';
import 'package:mtn_toth/loginandregistration.dart';

class SignUp extends StatelessWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Sign Up"),
          centerTitle: true,
        ),

        //body1
        body: Center(
            child: ElevatedButton(
          child: Text("Dashboard"),
          onPressed: () => {
            Navigator.push(
                context,
                new MaterialPageRoute(
                    builder: (context) => LoginAndRegistration()))
          },
        )));
  }
}
